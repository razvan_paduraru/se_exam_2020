public class Ex2 {

    public static void main(String[] args) {
        myThread t1 = new myThread("OThread1");
        myThread t2 = new myThread("OThread2");

        t1.start();
        t2.start();
    }
}

class myThread extends Thread{
    private String name;

    public myThread(String name) {
        this.name = name;
    }

    @Override
    public void run(){
        for (int i = 1; i <= 13; i++){
            System.out.println('['+ this.name + "] - [" + i + ']');
            try {
                Thread.sleep(3000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}


